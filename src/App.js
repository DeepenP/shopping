import React from 'react';
import './App.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { NavBar } from './component/NavBar'
import Footer from './component/Footer'
import ProductsContextProvider from './Store/ProductsContext';
import CartContextProvider from './Store/CartContext';
import Products from './component/Products';
import { Cart } from './component/Cart'
import { NotFoundError } from './component/NotFoundError'


export const App = () => {

    return (
        <div>
      
            <ProductsContextProvider>
                <CartContextProvider>
                    <Router>
                        <NavBar />
                       
                        <Switch>
                            <Route path='/' exact component={Products} />
                            <Route path='/cart' exact component={Cart} />
                            <Route component={NotFoundError} />
                        </Switch>
                    </Router>
                </CartContextProvider>
            </ProductsContextProvider>
            <Footer />
        </div>
    )
}
