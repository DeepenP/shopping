import React from'react'

export const NotFoundError=()=>{
    return(
        <div className='container'>
            <div className='not-found-error'>
                <h2>Error Page Not Found</h2>
            </div>
        </div>
    )
    }
