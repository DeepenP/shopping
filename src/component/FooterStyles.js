import styled from 'styled-components';

export const Box = styled.div`
padding: 60px 30px;
background: rgba(0,0,0,0.8);
position: relative;
bottom: 0;
width: 100%;


@media (max-width: 1000px) {
	padding: 50px 30px;
}
`;

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	max-width: 1000px;
	margin: 0 auto;
`

export const Column = styled.div`
display: flex;
flex-direction: column;
text-align: left;
margin-left: 50px;
`;

export const Row = styled.div`
display: grid;
grid-template-columns: repeat(auto-fill, minmax(190px, 1fr));
grid-gap: 20px;

@media (max-width: 1000px) {
	grid-template-columns: repeat(auto-fill,minmax(150px, 1fr));
}
`;

export const FooterLink = styled.a`
color: white;
margin-bottom: 20px;
font-size: 14px;
text-decoration: none;

&:hover {
	color: orangered;
	transition: 100ms ease-in;
}
`;

export const Heading = styled.p`
font-size: 22px;
color: white;
margin-bottom: 20px;
font-weight: bold;
`;
