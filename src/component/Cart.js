import React, { useContext } from 'react'
import { CartContext } from '../Store/CartContext'
import StripeCheckout from 'react-stripe-checkout'

export const Cart = () => {
    const { shoppingCart, totalPrice, qty, dispatch } = useContext(CartContext);
    
    console.log(shoppingCart);
    return (
        <div className="cart-container">
            <div className='cart-details' style={{ marginTop: '100px' }}>
                {shoppingCart.length > 0 ?
                    shoppingCart.map(cart => (
                        <div className='cart' key={cart.id}>
                            <div className='cart-image'><img src={cart.image} alt="mot found" /></div>
                            <span className='cart-product-name'>{cart.name}</span>
                            <span className='cart-product-price'>${cart.price}.00</span>
                            <span className='inc' onClick={() => dispatch({ type: 'INCREASE', id: cart.id, cart })}><i className="fas fa-plus"></i></span>
                            <span className='product-quantity'>{cart.qty}</span>
                            <span className='dec' onClick={() => dispatch({ type: 'DECREASE', id: cart.id, cart })}><i className="fas fa-minus"></i></span>
                            <span className='product-total-price'>${cart.price * cart.qty}.00</span>
                            <span className='delete-product' onClick={() => dispatch({ type: 'DELETE', id: cart.id, cart })}><i className="far fa-trash-alt"></i></span>

                        </div>
                    ))

                    : 'no item selected,your cart is empty'}
            </div>
            {shoppingCart.length > 0 ? <div className='cart-description'>
                <div className='description'>
                    <h3>Cart Total</h3>
                    <div className='total-items'>
                        <div className='items'>Total Items</div>
                        <div className='items-count'>{qty}</div>
                    </div>
                    <div className='total-price-section'>
                        <div className='title'>Total Price</div>
                        <div className='items-price'>${totalPrice}.00</div>
                    </div>
                    <div className='stripe-section'>
                        <StripeCheckout>
                            
                        </StripeCheckout>

                    </div>
                </div>
            </div> : ''}
        </div>
    )
}
