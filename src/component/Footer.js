import React from "react";
import {
Box,
Container,
Row,
Column,
FooterLink,
Heading,
} from "./FooterStyles";

const Footer = () => {
return (
	<Box>
	<h1 style={{ color: "orangered",
				textAlign: "center",
				marginTop: "-50px" }}>
		TechShop
	</h1>
	<br />
	<Container>
		<Row>
		<Column>
			<Heading>About Us</Heading>
			<FooterLink href="#">About TechShop</FooterLink>
			<FooterLink href="#">Blogs</FooterLink>
			<FooterLink href="#">Carrers</FooterLink>
			<FooterLink href="#">TechShop Devices</FooterLink>
		</Column>
		<Column>
			<Heading>Help</Heading>
			<FooterLink href="#">Payment</FooterLink>
			<FooterLink href="#">Shipping</FooterLink>
			<FooterLink href="#">Cancellation & return</FooterLink>
		</Column>
		<Column>
			<Heading>Contact Us</Heading>
			<FooterLink href="#">Kathmandu</FooterLink>
			<FooterLink href="#">Bhaktapur</FooterLink>
			<FooterLink href="#">Pokhara</FooterLink>
			<FooterLink href="#">Lalitpur</FooterLink>
		</Column>
		<Column>
			<Heading>Follow Us </Heading>
			<FooterLink href="#">
			<i className="fab fa-facebook-f">
				<span style={{ marginLeft: "10px" }}>
				Facebook
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-twitter">
				<span style={{ marginLeft: "10px" }}>
				Twitter
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-youtube">
				<span style={{ marginLeft: "10px" }}>
				Youtube
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-instagram">
				<span style={{ marginLeft: "10px" }}>
				Instagram
				</span>
			</i>
			</FooterLink>
		</Column>
		</Row>
	</Container>
	</Box>
);
};
export default Footer;
