import React from 'react'

 export const Banner =()=> {
    return (
            <header>
                <div className='headerText'>
                    <div>
                    <h1>Just a step away from home</h1>
                    <p>Enjoy shopping at TechShop</p>
                    </div>              
                </div>
            </header>
    )
}


