import React, { createContext, useState } from 'react'

import Headphone1 from '../images/img2.jpg'
import Camera1 from '../images/img3.jpg'
import Watch1 from '../images/img4.jpg'
import Laptop from '../images/img5.jpg'
import Mobile1 from '../images/img6.jpg'
import Headphone2 from '../images/img7.jpg'
import Watch2 from '../images/img8.jpg'
import Camera2 from '../images/img9.jpg'
import Mobile2 from '../images/img10.jpg'


export const ProductsContext = createContext();
const ProductsContextProvider = (props) => {
    const [products] = useState([
        { id: 1, name: 'Headphone1', price: 300, image: Headphone1, status: 'popular' },
        { id: 2, name: 'Camera1', price: 700, image: Camera1, status: 'new' },
        { id: 3, name: 'Watch1', price: 200, image: Watch1, status: 'new' },
        { id: 4, name: 'Laptop', price: 10000, image: Laptop, status: 'popular' },
        { id: 5, name: 'Mobile1', price: 5000, image: Mobile1, status: 'new' },
        { id: 6, name: 'Headphone2', price: 400, image: Headphone2, status: 'new' },
        { id: 7, name: 'Watch2', price: 350, image: Watch2, status: 'popular' },
        { id: 8, name: 'Camera2', price: 20000, image: Camera2, status: 'popular' },
        { id: 9, name: 'Mobile2', price: 7500, image: Mobile2, status: 'popular' }

    ]);
    return (
        <ProductsContext.Provider value={{ products: [...products] }}>
            {props.children}

        </ProductsContext.Provider>
    )
}

export default ProductsContextProvider;
